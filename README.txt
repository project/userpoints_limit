
DESCRIPTION
------------
 This module is used to extend the userpoint functionality.
 It Limit the user points in certain period. The admin can set a provision to
 limit the points a user can acquire for a certain period,
 such as:

  - posting a node (different points can be awarded for different
   node types, e.g. page, story, forum, image, ...etc.)
  - posting a comment
  - moderating a comment

Dependencies
------------
 1) userpoint module (http://drupal.org/project/userpoints)

Installation
------------
To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the userpoints_limit directory and all its contents to your
   modules directory.

Configuration
-------------
To enable this module do the following:

1. Go to Admin -> Modules, and enable userpoints_limit.
   Check the messages to make sure that you did not get any errors
   on database creation.

2. Go to Admin -> Settings -> userpoints.

   Configure the options as per your requirements


CREDITS
----------------------------
Authored and maintained by jaffaralia
