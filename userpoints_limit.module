<?php

/**
 * @file
 * The userpoint limit, which stops the userpoint before reached certain limits.
 */

/**
 * Implements hook_help().
 */
function userpoints_limit_help($path, $arg) {
  switch ($path) {
    case 'admin/help#userpoints_limit':
      $output['header'] = array('#markup' => '<h3>' . t('About') . '</h3> <br />');
      $output['header_content'] = array('#markup' => t('This module allows an admin to configure a limit on User Points for a certain defined window.'));
      $output['header_content_links'] = array('#markup' => '<br /><h3>' . t('Userpoints Limit administration pages') . '</h3>');
      $link_list['point_settings'] = l(t('Points'), 'admin/config/people/userpoints/settings');
      $output['topic-list'] = array('#markup' => theme('item_list', array('items' => $link_list)));
      return render($output);
  }
}

/**
 * Implements hook_userpoints().
 */
function userpoints_limit_userpoints($op, $params = array()) {
  switch ($op) {
    case 'setting':
      $intervals = array(
        // 1-3 days.
        86400, 86400 * 2, 86400 * 3,
        // 1, 2, 3 weeks.
        604800, 604800 * 2, 604800 * 3,
        // 1, 3 months.
        86400 * 30, 86400 * 90,
      );
      $intervals = drupal_map_assoc(variable_get('userpoints_limit_interval', $intervals), 'format_interval');
      $form['userpoint_limit'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Userpoints Limit'),
        '#group' => 'settings_additional',
        '#weight' => 25,
      );
      $form['userpoint_limit']['userpoints_limit_duration'] = array(
        '#type' => 'select',
        '#title' => t('Duration'),
        '#default_value' => variable_get('userpoints_limit_duration', 0),
        '#options' => $intervals,
        '#description' => t('Reset Userpoint Duration'),
      );
      $form['userpoint_limit']['userpoints_limit_message'] = array(
        '#type' => 'textarea',
        '#title' => t('Message'),
        '#default_value' => variable_get('userpoints_limit_message', 'You may accrue a maximum of !limit points during each !duration period.'),
        '#description' => t('The message a user will see if they exceed the limit. Available replacement values include !duration and !limit.'),
      );
      $form['userpoint_limit']['userpoints_limit_points'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Points Limit'),
        '#default_value' => variable_get('userpoints_limit_points', -1),
        '#description' => t('Maximum number of points allowed for each user. Set to -1 to allow unlimted points.'),
        '#size'          => 5,
      );
      return $form;

    case 'points before':
      global $user;
      // Get time duration value and reduce -1
      // for 23 hours: 59 minutes: 59 seconds.
      if (($duration = (variable_get('userpoints_limit_duration', 0) - 1)) && variable_get('userpoints_limit_points', -1) != -1) {
        $last_timestamp = userpoints_limit_generate_query($params);

        /* Enforce consistent sort order.
         * Check time limit extends the duration
         * for eg: if user logged in 1'st day. till 2nd day he didnt logout but
         * the limit is one day.. so, we need to check and reset
         * userpoints_limits table.
         */
        if ($last_timestamp) {
          if ((mktime(0, 0, 0, date('m', $last_timestamp), date('d', $last_timestamp), date('y', $last_timestamp)) + $duration) < (mktime(0, 0, 0) + $duration)) {
            $limit['uid'] = $params['uid'];
            $limit['timestamp'] = time();
            $ret = drupal_write_record('userpoints_limits', $limit, array('uid'));
            $last_timestamp = $limit['timestamp'];
          }
        }
        else {
          $limit['uid'] = $params['uid'];
          $limit['timestamp'] = time();
          $ret = drupal_write_record('userpoints_limits', $limit);
          $last_timestamp = $limit['timestamp'];
        }
        $settings = array();
        $settings['max_points'] = variable_get('userpoints_limit_points', -1);
        $settings['duration'] = variable_get('userpoints_limit_duration', 0);
        $from_time = mktime(0, 0, 0, date('m', $last_timestamp), date('d', $last_timestamp), date('y', $last_timestamp));
        $to_time = $from_time + $duration;
        $result = db_select('userpoints_txn', 'p')
                  ->condition('p.uid', $params['uid'], '=')
                  ->condition('p.changed', array($from_time, $to_time), 'BETWEEN');
        $result->addExpression('SUM(points)', 'points_cnt');
        $result = $result->execute()
                  ->fetchObject();
        if (isset($result) && (($result->points_cnt + $params['points']) > variable_get('userpoints_limit_points', -1))) {
          // Stop userpoints before store
          // check if userpoints is deducted.
          if ($params['points'] > 0) {
            $message = userpoints_limit_message_content($settings);
            // Do not allow the points transaction.
            drupal_set_message(check_plain($message), 'error');
            return FALSE;
          }
          else {
            $query_settings = array();
            $query_settings['query_values'] = $params;
            $query_settings['from_time'] = $from_time;
            $query_settings['to_time'] = $to_time;
            $query_settings['settings'] = $settings;
            if (!userpoints_limit_points_query($query_settings)) {
              return FALSE;
            }
          }
        }
        elseif ($params['points'] < 0) {
          // Userpoints below zero.
          $query_settings = array();
          $query_settings['query_values'] = $params;
          $query_settings['from_time'] = $from_time;
          $query_settings['to_time'] = $to_time;
          $query_settings['settings'] = $settings;
          if (!userpoints_limit_points_query($query_settings)) {
            return FALSE;
          }
        }
      }
      break;
  }
}

/**
 * Generate userpoints_limits query.
 *
 * @param array $params
 *   It contains query arguments.
 *
 * @return object
 *   It contains query object
 */
function userpoints_limit_generate_query($params) {
  $result = db_select('userpoints_limits', 'p')
            ->fields('p')
            ->condition('p.uid', $params['uid'], '=')
            ->execute()
            ->fetchObject();
  if ($result) {
    return $result->timestamp;
  }
  return FALSE;
}

/**
 * Check if the userpoint is deducted.
 *
 * @param array $query_settings
 *   query_values => It contains query arguments and message settings
 *   from_time => from time when the points inserted
 *   to_time => to time
 *
 * @return bool
 *   if deducted points count not available then it retunrs false.
 */
function userpoints_limit_points_query($query_settings) {
  $deduct_point = db_select('userpoints_txn', 'p')
                  ->condition('p.uid', $query_settings['query_values']['uid'], '=')
                  ->condition('p.entity_type', $query_settings['query_values']['entity_type'], '=')
                  ->condition('p.entity_id', $query_settings['query_values']['entity_id'], '=')
                  ->condition('p.changed', array($query_settings['from_time'], $query_settings['to_time']), 'BETWEEN');
  $deduct_point->addExpression('COUNT(*)', 'count');
  $deduct_point = $deduct_point->execute()
                  ->fetchObject();
  if (!$deduct_point->count) {
    // Stop userpoints before store.
    $message = userpoints_limit_message_content($query_settings['settings']);
    // Do not allow the points transaction.
    drupal_set_message(check_plain($message), 'error');
    return FALSE;
  }
}

/**
 * To construct message.
 *
 * @param array $settings
 *   It's a array value category => user points category name
 *   max_points => It contains maximum points for the user
 *   tid => It contains taxonomy id
 *
 * @return message
 *   It returns the confirmation message
 */
function userpoints_limit_message_content($settings) {
  $message = strtr(variable_get('userpoints_limit_message', 'You may accrue a maximum of !limit points during each !duration period.'), array(
      '!limit' => $settings['max_points'],
      '!duration' => current(drupal_map_assoc(array($settings['duration']), 'format_interval')),
    )
  );
  return $message;
}

/**
 * Implements hook_user_login().
 */
function userpoints_limit_user_login(&$edit, $account) {
  global $user;
  if (($interval = variable_get('userpoints_limit_duration', 0)) && variable_get('userpoints_limit_points', -1) != -1) {
    $last_timestamp = db_select('userpoints_limits', 'p')
                      ->fields('p')
                      ->condition('p.uid', $user->uid, '=')
                      ->execute()
                      ->fetchObject();
    if (isset($last_timestamp->timestamp) && $last_timestamp->timestamp) {
      // See if it's time for a reset.
      if ((time() - $interval) >= $last_timestamp->timestamp) {
        $limit['uid'] = $user->uid;
        $limit['timestamp'] = time();
        $ret = drupal_write_record('userpoints_limits', $limit, array('uid'));
      }
    }
    else {
      $limit['uid'] = $user->uid;
      $limit['timestamp'] = time();
      $ret = drupal_write_record('userpoints_limits', $limit);
    }
  }
}

/**
 * Implements hook_user_delete().
 */
function userpoints_limit_user_delete($account) {
  // The user is being deleted, delete all traces in userpoints limit table.
  db_delete('userpoints_limits')
          ->condition('uid', $account->uid)
          ->execute();
}
